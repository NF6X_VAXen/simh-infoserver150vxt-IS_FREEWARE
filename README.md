# simh-infoserver150vxt-FREEWARE: Simulation of DEC InfoServer 150VXT providing OpenVMS freeware CD-ROMs

This is a pre-configured [SIMH](https://github.com/simh/simh) instance to simulate a [Digital Equipment Corporation (DEC)](https://en.wikipedia.org/wiki/Digital_Equipment_Corporation) InfoServer 150VXT system, serving OpenVMS freeware CD-ROMs over a LAN. This uses non-routable protocols such as [MOP](https://en.wikipedia.org/wiki/Maintenance_Operations_Protocol) and LAD, so it only works when the InfoServer emulation and any of its clients are on the same LAN.

A directory of the freeware CD contents may be found at:

https://wiki.vmssoftware.com/Freeware_CD 

The .iso images here were recreated from the zip archives found at:

https://www.digiater.nl/openvms/freeware/disks/

Thus, these .iso files here are not block-level images of the original CD-ROMs, so at the very least their volume and volume set labeling probably differ from the originals. Some of them are too large to fit on a physical CD-ROM, so I speculate that some of the original ones were split across multiple discs. But we are not constrained by the same physical hardware limitations in this simulation, and the large .iso images seem to work.

This simulation was set up using the InfoServer software provided on the OpenVMS Freeware v80 CD-ROM which is included here. I have also archived InfoServer software and documentation for convenience [here](https://gitlab.com/NF6X_VAXen/InfoServer-docs), and OpenVMS 7.3 documentation [here](https://gitlab.com/NF6X_VAXen/OpenVMS-v7.3-docs).


## System Configuration

Description            | Default setting
-----------------------|------------------
Server name            | IS_FREEWARE
Administrator password | ess
Network interface      | vde:/var/run/vde2/tap1.ctl


## Using the Simulation

Before you can run this simulation instance, you will need to download and install the [SIMH](https://github.com/simh/simh) software, particularly the `infoserver150vxt` simulator. You will probably need to edit the initialization file provided here to configure networking for your host system. The initialization file assumes that you will be using a [Virtual Distributed Ethernet](https://github.com/virtualsquare/vde-2) switch with its control port located at `/var/run/vde2/tap1.ctl`. Depending on your host operating system, you may need to use a different network configuration. SIMH network configuration is described in the [0readme_ethernet.txt](https://github.com/simh/simh/blob/master/0readme_ethernet.txt) file provided with SIMH.

To launch the simulation:

   infoserver150vxt IS_FREEWARE.ini

To shut it down, log in with the administrator password, either on the console or via [LAT](https://en.wikipedia.org/wiki/Local_Area_Transport). Then use the `shutdown` command.

You might find it helpful to run the simulation in a detached `screen` session. The included script `start-screen.sh` starts the simulator with its console in a `screen` session, with the `screen` escape key set to `^p` so that it does not conflict with SIMH's use of `^e` as its escape key.

An included DCL script shows how to mount disks from the InfoServer simulation under OpenVMS.


## File Manifest

Filename                     | Description
-----------------------------|--------------------------------------------
FREEWAREV10.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 1
FREEWAREV20.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 2
FREEWAREV30.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 3
FREEWAREV40.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 4
FREEWAREV50.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 5
FREEWAREV60.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 6
FREEWAREV70.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 7
FREEWAREV80.iso              | Reconstructed OpenVMS freeware CD-ROM image, version 8
IS_FREEWARE.ini              | SIMH `infoserver150vxt` initialization file
IS_FREEWARE.nvr              | InfoServer nonvolatile RAM image
IS_FREEWARE_system_rz1.rz24  | InfoServer system disk image
MOUNT_FREEWARE.COM           | Example DCL script to mount CD-ROMs from InfoServer
README.md                    | This file
start-screen.sh              | Example shell script to launch simulator in a detachable `screen` session
